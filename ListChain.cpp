#include "ListChain.h"

ListChain::ListChain(double factor, int xDegree, int yDegree, int zDegree) : data(factor, xDegree, yDegree, zDegree), nextChain(nullptr) {

}
ListChain::ListChain(const MonomStruct& monom) : data(monom), nextChain(nullptr) {

}

MonomStruct& ListChain::getMonomStruct() {
    return data;
}
void ListChain::setMonomStruct(MonomStruct& monom) {
    data = monom;
}

void ListChain::setNextChain(ListChain* _nextChain) {
    nextChain = _nextChain;
}
ListChain* ListChain::getNextChain() {
    return nextChain;
}
