#include "MainWindow.h"
#include "ui_mainwindow.h"

#include "List.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , degreeIntValidator(0,9,this)
{
    ui->setupUi(this);
    ui->isFirstList->setChecked(true);

    ui->sumInFirstList->setEnabled(false);
    ui->sumInNewList->setEnabled(false);

    ui->monomDegreeX->setValidator(&degreeIntValidator);
    ui->monomDegreeY->setValidator(&degreeIntValidator);
    ui->monomDegreeZ->setValidator(&degreeIntValidator);
}

MainWindow::~MainWindow()
{
    delete ui;
}

List* userList = new List[2];
short int currentList = 0;

void MainWindow::on_addMonomToList_clicked()
{

    if ( !ui->monomFactor->text().isEmpty() ) {
        double userFactor = ui->monomFactor->text().toDouble();

        int userDegreeX = ui->monomDegreeX->text().toInt();
        int userDegreeY = ui->monomDegreeY->text().toInt();
        int userDegreeZ = ui->monomDegreeZ->text().toInt();

        userList[currentList].addToList( MonomStruct(userFactor, userDegreeX, userDegreeY, userDegreeZ) );

        ui->sumInFirstList->setEnabled(true);
        ui->sumInNewList->setEnabled(true);
    }

    if (!currentList) {
        ui->monomOutputFirst->setText("");
    }
    else ui->monomOutputSecond->setText("");

    for (int i = 0; i < userList[currentList].getSizeList()-1; i++) {
        MonomStruct result = userList[currentList].getMonomStruct(i);

        if (!currentList) ui->monomOutputFirst->setText(  ui->monomOutputFirst->text() + QString("%1 * x^%2 y^%3 z^%4").arg( QString::number(result.factor), QString::number(result.getVariableDegree(variableDegree::x)), QString::number(result.getVariableDegree(variableDegree::y)), QString::number(result.getVariableDegree(variableDegree::z)) + " + " ));
        else  ui->monomOutputSecond->setText(  ui->monomOutputSecond->text() + QString("%1 * x^%2 y^%3 z^%4").arg( QString::number(result.factor), QString::number(result.getVariableDegree(variableDegree::x)), QString::number(result.getVariableDegree(variableDegree::y)), QString::number(result.getVariableDegree(variableDegree::z)) + " + " ));
    }

    MonomStruct result = userList[currentList].getMonomStruct(userList[currentList].getSizeList()-1);
    if (!currentList) ui->monomOutputFirst->setText(  ui->monomOutputFirst->text() + QString("%1 * x^%2 y^%3 z^%4").arg( QString::number(result.factor), QString::number(result.getVariableDegree(variableDegree::x)), QString::number(result.getVariableDegree(variableDegree::y)), QString::number(result.getVariableDegree(variableDegree::z)) ));
    else  ui->monomOutputSecond->setText(  ui->monomOutputSecond->text() + QString("%1 * x^%2 y^%3 z^%4").arg( QString::number(result.factor), QString::number(result.getVariableDegree(variableDegree::x)), QString::number(result.getVariableDegree(variableDegree::y)), QString::number(result.getVariableDegree(variableDegree::z)) ));

}

void MainWindow::on_isSecondList_clicked()
{
    ui->addMonomBox->setTitle("Добавить моном в полином №2");
    currentList = 1;
}

void MainWindow::on_isFirstList_clicked()
{
    ui->addMonomBox->setTitle("Добавить моном в полином №1");
    currentList = 0;
}

void MainWindow::on_sumInFirstList_clicked()
{
    ui->monomOutputFirst->setText("");
    ui->monomOutputSecond->setText("");

    userList[0].sumOfLists(userList[1]);

    for (int i = 0; i < userList[0].getSizeList()-1; i++) {
        MonomStruct result = userList[0].getMonomStruct(i);

        ui->monomOutputFirst->setText(  ui->monomOutputFirst->text() + QString("%1 * x^%2 y^%3 z^%4").arg( QString::number(result.factor), QString::number(result.getVariableDegree(variableDegree::x)), QString::number(result.getVariableDegree(variableDegree::y)), QString::number(result.getVariableDegree(variableDegree::z)) + " + " ));
    }

    for (int i = 0; i < userList[1].getSizeList()-1; i++) {
        MonomStruct result = userList[1].getMonomStruct(i);

       ui->monomOutputSecond->setText(  ui->monomOutputSecond->text() + QString("%1 * x^%2 y^%3 z^%4").arg( QString::number(result.factor), QString::number(result.getVariableDegree(variableDegree::x)), QString::number(result.getVariableDegree(variableDegree::y)), QString::number(result.getVariableDegree(variableDegree::z)) + " + " ));
    }

    {
    MonomStruct result = userList[0].getMonomStruct(userList[0].getSizeList()-1);
    ui->monomOutputFirst->setText(  ui->monomOutputFirst->text() + QString("%1 * x^%2 y^%3 z^%4").arg( QString::number(result.factor), QString::number(result.getVariableDegree(variableDegree::x)), QString::number(result.getVariableDegree(variableDegree::y)), QString::number(result.getVariableDegree(variableDegree::z)) ));
    }

    MonomStruct result = userList[1].getMonomStruct(userList[1].getSizeList()-1);
    ui->monomOutputSecond->setText(  ui->monomOutputSecond->text() + QString("%1 * x^%2 y^%3 z^%4").arg( QString::number(result.factor), QString::number(result.getVariableDegree(variableDegree::x)), QString::number(result.getVariableDegree(variableDegree::y)), QString::number(result.getVariableDegree(variableDegree::z)) ));
}

void MainWindow::on_sumInNewList_clicked()
{
    ui->sumOfTwoLists->setText("");

    List sumOfLists = userList[0] + userList[1];

    for (int i = 0; i < sumOfLists.getSizeList()-1; i++) {
        MonomStruct result = sumOfLists.getMonomStruct(i);

        ui->sumOfTwoLists->setText(  ui->sumOfTwoLists->text() + QString("%1 * x^%2 y^%3 z^%4").arg( QString::number(result.factor), QString::number(result.getVariableDegree(variableDegree::x)), QString::number(result.getVariableDegree(variableDegree::y)), QString::number(result.getVariableDegree(variableDegree::z)) + " + " ));
    }

    MonomStruct result = sumOfLists.getMonomStruct(sumOfLists.getSizeList()-1);
    ui->sumOfTwoLists->setText(  ui->sumOfTwoLists->text() + QString("%1 * x^%2 y^%3 z^%4").arg( QString::number(result.factor), QString::number(result.getVariableDegree(variableDegree::x)), QString::number(result.getVariableDegree(variableDegree::y)), QString::number(result.getVariableDegree(variableDegree::z)) ));
}
