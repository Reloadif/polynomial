#ifndef LISTCHAIN_H
#define LISTCHAIN_H

#include "MonomStruct.h"

class ListChain {
    MonomStruct data;
    ListChain* nextChain;

public:
    ListChain(double factor, int xDegree, int yDegree, int zDegree);
    ListChain(const MonomStruct& monom);

    MonomStruct& getMonomStruct();
    void setMonomStruct(MonomStruct& monom);

    void setNextChain(ListChain* _nextChain);
    ListChain* getNextChain();
};

#endif // LISTCHAIN_H
