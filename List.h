#ifndef LIST_H
#define LIST_H

#include "ListChain.h"

class List {
    ListChain* startChain;

    int realElements;
public:
    List();
    ~List();

    MonomStruct removeFromList(const int index);
    void addToList(const MonomStruct& monom);

    ListChain* searchInList(const int compoundDegree, bool& isFind);

    int getSizeList();
    bool isEmpty();

    MonomStruct getMonomStruct(const int index);

    void sumOfLists(List& list);
    List operator+(List& list);

    List& operator=(List& list);
};

#endif // LIST_H
