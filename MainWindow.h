#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QIntValidator>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_addMonomToList_clicked();

    void on_isSecondList_clicked();

    void on_isFirstList_clicked();

    void on_sumInFirstList_clicked();

    void on_sumInNewList_clicked();

private:
    Ui::MainWindow *ui;
    QIntValidator degreeIntValidator;
};
#endif // MAINWINDOW_H
