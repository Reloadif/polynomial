#include "MonomStruct.h"

int MonomStruct::calculateCompoundDegree(int xDegree, int yDegree, int zDegree) {
    return 100 * xDegree + 10 * yDegree + zDegree;
}

MonomStruct::MonomStruct(double _factor, int xDegree, int yDegree, int zDegree) : factor(_factor), compoundDegree(calculateCompoundDegree(xDegree, yDegree, zDegree)) {

}
MonomStruct::MonomStruct(const MonomStruct& monom) : factor(monom.factor), compoundDegree(monom.compoundDegree) {

}

int MonomStruct::getVariableDegree(variableDegree variable) {
    int result = 0;

    switch (variable)
    {
    case variableDegree::x:
        result = compoundDegree / 100;
        break;
    case variableDegree::y:
        result = compoundDegree % 100 / 10;
        break;
    case variableDegree::z:
        result = compoundDegree % 10;
        break;
    default:
        result = -1;
    }

    return result;
}

MonomStruct& MonomStruct::operator=(MonomStruct& monom) {
    if (this == &monom) {
        return *this;
    }
    this->factor = monom.factor;
    this->compoundDegree = monom.compoundDegree;
    return *this;
}
