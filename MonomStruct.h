#ifndef MONOMSTRUCT_H
#define MONOMSTRUCT_H

enum class variableDegree {
    x,
    y,
    z
};

struct MonomStruct {
private:
    int calculateCompoundDegree(int xDegree, int yDegree, int zDegree);

public:
    double factor;
    int compoundDegree;

    MonomStruct(double _factor, int xDegree, int yDegree, int zDegree);
    MonomStruct(const MonomStruct& monom);

    int getVariableDegree(variableDegree variable);

    MonomStruct& operator=(MonomStruct& monom);
};

#endif // MONOMSTRUCT_H
